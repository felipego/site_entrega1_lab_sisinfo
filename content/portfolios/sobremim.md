+++
title = 'Sobre mim'
date = 2024-10-13T09:41:33-03:00
draft = false
+++
Meu nome é Felipe Go U. de la R., nascido em 2004 na cidade de São Paulo, onde moro até hoje. 


Atualmente sou um estudante do terceiro ano de Engenharia Mecatrônica pela Escola Politécnica da USP.

Apesar de ainda ser um estudante, já tenho alguma experiência profissional na área de energia renovável com o summer job que fiz antes de entrar na universidade em que pude ver de perto como é o desenvolvimento de uma planta de painéis solares.

No futuro, planejo ter uma experiência de estudo fora do país a partir do program de Aproveitamento de Estudo fornecido pela Poli ou fazer uma pós-graduação no estrangeiro.

A partir dos estudos eu espero continuar a desenvolver meu interesse por tecnologia, especialmente por meio de sistemas como Internet das Coisas (IoT). 


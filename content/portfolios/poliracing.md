+++
title = 'Equipe Poli Racing'
date = 2024-10-13T09:40:53-03:00
draft = false
+++
### Duração: 2023 - Presente

### Trainee:2023 - 2024

### Projetista:2024 - Presente

### Subsistema: Frame & Body (Chassi)

---

### O que é?

Equipe de Fórmula SAE da Poli

---

### Por quê Poli Racing?

Durante a minha graduação, eu percebi que apesar do conhecimento teórico que eu aprendia, eu sentia falta de poder aplicar esses conhecimentos em um cenário mais realista e também ter uma forma de visualizar de forma prática o meu conhecimento, tal como o proporcionado pela Poli Racing, além de que eu sempre tive um interesse pelo assunto de carros de fórmula e queria saber mais sobre isso.

---

### Projetos que participei:

- Gabarito do chassi (Manufatura):
    O gabarito é uma estrutura, geralmente feita de madeira ou outro material comum e barato, que é utilizada como referência para o posicionamento das peças/tubos que vão compor o chassi, de modo a garantir maior precisão e facilidade para a montagem e junção das peças que formam a estrutura do chassi do carro.

    Nesse projeto, eu fui um dos responsáveis pela fabricação da estrutura do gabarito, tanto no corte a laser das madeiras que compôe o gabarito, quanto na montagem do mesmo para uso.

- Banco: 
    Nesse projeto fui designado para idealizar e fabricar o banco que seria utilizado pelos pilotos enquanto dirigiam o carro

    Para isso eu decidi fazer um banco de espuma expansiva (Poliuretano), que é um composto químico dividido em partes A e B, que ao serem misturadas, esquentam e expandem, tomando a forma do molde em que a mistura foi feita. Desse modo, ao misturar o Poliuretano eu o coloquei em um saco plástico dentro do carro e debaixo do piloto, de modo que a espuma tomasse a forma de um banco cujo formato encaixasse exatamente com a silhueta das costas e glúteos do piloto.

- Mockup (em andamento): 
    O Mockup é uma estrutura ajustável cujo objetivo é simular o posicionamento dos pedais,volante,banco dentro de um cockpit, para que se possa verificar empiricamente quais são as posições mais confortáveis ao piloto, que serão usadas de referência para a montagem da estrutura real do carro.
    
    Como o projeto ainda está em andamento não foi definido qual será a solução utilizada.

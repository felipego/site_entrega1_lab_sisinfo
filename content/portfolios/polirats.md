+++
title = 'Poli Rats'
date = 2024-10-13T09:41:17-03:00
draft = false
+++
### Duração: 2022 - Presente

### Diretor de Modalidade:2023-2025

---

### O que é?

Equipe de futebol americano (flag footbal) da Poli/USP

![Logo do Poli Rats](/static/images/logo_polirats.jpg)

---

### O que é Flag Footbal?

Flag footbal (5x5) é uma modalidade (que será olímpica em 2026) derivada do futebol americano, com a principal diferença que o flag footbal é um esporte com menos contato do que o o futebol americano.

[Para saber mais sobre o flag footbal e suas regras, clique aqui](https://nflflag.com/coaches/flag-football-rules)

---

### Campeonatos que disputei:
- Paulista de Flag x8: 2022
- Liga Universitária de Futebol Americano x8: 2022
- Paulista de flag x8: 2023
- Liga Universitária de Futebol Americano x8: 2023
- TUSCA x8: 2023
- Paulista de Flag x5: 2024
- Liga Universitária de Futebol Americano x5: 2024 **(Campeão)** 

---

### Por quê Poli Rats?

Logo após entrar na USP, uma das primeiras coisas que eu notei foi a quantidade de equipes que possibilitavam a prática dos mais diversos esportes. 

Assim, querendo aproveitar essa oportunidade, decidi testar um treino na equipe de Futebol Americano, um esporte que sempre admirei, mas nunca tive a chance de praticar, visto que ainda é um esporte menos conhecido no Brasil. No fim, acabei gostando tanto que estou no time até hoje.

---

### Minha evolução dentro e fora dos gramados:

Quanto entrei no time, apesar de já acompanhar o esporte, eu não tinha a mínima noção de como era realmente jogá-lo, todos os novos conceitos e nomes que eu tinha que aprender junto à adaptação a uma rotina de treinos do time, somado à necessidade de me adaptar a uma nova vida na faculdade, não posso negar que meu primeiro ano e meio no time fosse mais de aprendizado do que efetivamente jogar os jogos e campeonatos ou ter uma participação ativa nas questões do time.

Contudo, depois desse período de adaptação, eu finalmente me sentia confortável para evoluir no time tanto dentro quanto fora dos gramados. No campo, eu comecei a treinar e jogar cada vez mais partidas e campeonatos a partir de 2024. E fora dele, eu assumi no fim de 2023 o cargo de diretor de modalidade, que significava que eu e outros dois colegas eramos responsáveis por manter o funcionamento dos times como arranjar treinadores, campeonatos e visualizar qual seria o futuro da equipe.

Por conta disso, creio que um dos momentos mais marcantes da minha gestão como diretor de modalidade foi ter decidido transicionar em 2024 da modalidade 8x8, que o time praticava a anos, para 5x5 principalmente pelo fato de que ao ser anunciado como um esporte olímpico, a modalidade 5x5 tem uma previsão de crescimento no Brasil cada vez maior, aumentando o potencial do time e das oportunidades que isso pode trazer.

Assim, me orgulho de dizer que apesar de ter sido uma aposta arriscada ao sair da zona de conforto da equipe, posso dizer que essa escolha já está dando frutos como logo no nosso primeiro ano já ganhamos um campeonato, algo que já não acontecia há alguns anos na equipe.

![Eu com o troféu e medalha de campeão](/static/images/foto_eucomtaçalufa.jpg)

---

Por fim, apesar da minha história no Poli Rats ainda não ter terminado, creio que já não sou o mesmo que entrou na equipe em 2022, evolui não somente nos aspectos técnicos, mas também nos mentais, como liderança e saber tomar decisões difíceis, que eu vou levar pro resto da vida. 

Desse modo, eu já não me vejo mais como aquele calouro que acabou de entrar no time e está perdido, mas sim como um veterano que pode ajudar aqueles que acabaram de entrar na equipe e estão passando pelo que passei a cresceram tanto ou mais quanto eu como jogadores, mas também como pessoas.